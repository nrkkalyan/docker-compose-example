package com.nrk.springexamples.common.filter;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.nrk.springexamples.common.service.TracerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Order(1)
public class LogContextFilter implements Filter{
	
	private static final String LOG_TRACE_ID = "logTraceId";
	@Autowired
	private TracerService tracerService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final LocalDateTime startTime = LocalDateTime.now();
		MDC.put(LOG_TRACE_ID, UUID.randomUUID().toString().replaceAll("-",""));
		log.info("Inside LogContextFilter.doFilter method: TraceId:{}", tracerService.getTraceId());
		try{
			chain.doFilter(request, response);
		}finally {
			final String timeTaken = Duration.between(startTime, LocalDateTime.now()).toString().replace("PT","");
			log.info("Total time to execute:{}: TraceId:{}",timeTaken, tracerService.getTraceId());
			MDC.remove(LOG_TRACE_ID);
		}
	}

}
