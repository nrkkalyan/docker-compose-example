package com.nrk.springexamples.common.exception.advice;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.nrk.springexamples.common.service.TracerService;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ExceptionAdvice {

	@Autowired
	private TracerService tracerService;
	@Autowired
	private Environment environment;

	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Map<String, Serializable>> handleConflict(final Exception ex) {
		log.error("Exception occured", ex);
		final Map<String, Serializable> response = new HashMap<>();
		response.put("TraceId", tracerService.getTraceId());
		response.put("appId", environment.getRequiredProperty("spring.application.name"));
		response.put("Exception", ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
}
