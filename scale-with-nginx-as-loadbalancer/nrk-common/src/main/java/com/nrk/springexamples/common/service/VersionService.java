package com.nrk.springexamples.common.service;

import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.nrk.springexamples.common.utils.IoTools;

@Service
public class VersionService {

	
	public Properties applicationDetails() throws Exception {
		
		final Properties properties = new Properties();
        final InputStream resourceAsStream = IoTools.getResourceAsStream("git.properties");
        if (resourceAsStream != null) {
            properties.load(resourceAsStream);
        }
        
        return properties;
		
	}
	
}
