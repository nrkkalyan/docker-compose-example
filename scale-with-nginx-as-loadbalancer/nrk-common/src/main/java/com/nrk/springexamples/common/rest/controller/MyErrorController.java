package com.nrk.springexamples.common.rest.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.nrk.springexamples.common.service.TracerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class MyErrorController implements ErrorController {

	private static final String PATH = "/error";

	@Autowired
	private ErrorAttributes errorAttributes;
	@Autowired
	private TracerService tracerService;
	@Autowired
	private Environment environment;

	@RequestMapping(PATH)
	@ResponseBody
	public ResponseEntity<Map<String, Serializable>> processError(HttpServletRequest request, WebRequest webRequest) {
		log.error("Unexpected error occured. TraceId:{}", tracerService.getTraceId());
		// Get error status code.
		final Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		final String message = (String) request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
		final Exception exception = (Exception) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
		final Throwable trace = errorAttributes.getError(webRequest);

		final Map<String, Serializable> response = new HashMap<>();
		
		response.put("TraceId", tracerService.getTraceId());
		response.put("appId", environment.getRequiredProperty("spring.application.name"));
		response.put("Status Code", statusCode);

		if (StringUtils.isNotBlank(message)) {
			response.put("message", message);
		}

		if (trace != null) {
			response.put("StackTrace", ExceptionUtils.getRootCauseMessage(trace));
		} else if (exception != null) {
			response.put("StackTrace", ExceptionUtils.getRootCauseMessage(exception));
		}

		return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));

	}

	@Override
	public String getErrorPath() {
		return PATH;
	}

}
