package com.nrk.springexamples.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

public class ClassPathResourceLoader {

    private final List<ClassLoader> classLoaders = new ArrayList<>();

    public ClassPathResourceLoader() {
        classLoaders.add(Thread.currentThread().getContextClassLoader());
        classLoaders.add(getClass().getClassLoader());
        classLoaders.add(ClassLoader.getSystemClassLoader());
    }

    public InputStream getResourceAsStream(final String path) throws Exception {
        final URL resourceUrl = getResourceUrl(path);
        Validate.notNull(resourceUrl, path + " path not found in the classpath.");
        return getResourceAsStream(resourceUrl);
    }

    public InputStream getResourceAsStream(final URL resourceUrl) {
        try {
            Validate.notNull(resourceUrl, "ResourceUrl is null");
            return resourceUrl.openStream();
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public URL getResourceUrl(final String path) throws Exception {
        for (final ClassLoader classLoader : classLoaders) {
            final URL url = classLoader.getResource(StringUtils.removeStart(path, "/"));
            if (url != null) {
                return url;
            }
        }
        final File file = new File(path);
        Validate.isTrue(file.exists(), "The give file path does not exists. %s", path);
        return file.toURI().toURL();
    }

    public boolean isResourceExists(final String path) throws Exception {
        for (final ClassLoader classLoader : classLoaders) {
            final URL url = classLoader.getResource(StringUtils.removeStart(path, "/"));
            if (url != null) {
                return true;
            }
        }
        return new File(path).exists();
    }

    public List<ClassLoader> getClassLoaders() {
        return classLoaders;
    }

}
