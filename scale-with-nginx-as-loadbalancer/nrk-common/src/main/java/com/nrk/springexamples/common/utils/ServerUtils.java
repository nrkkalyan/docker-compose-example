package com.nrk.springexamples.common.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServerUtils {
	
	public static String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			log.error("", e);
			throw new IllegalStateException(e.getMessage());
		}
	}
}
