package com.nrk.springexamples.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import brave.Span;
import brave.Tracer;

@Service
public class TracerService {

	@Autowired
	private Tracer tracer;

	public String getTraceId() {

		final Span currentSpan = tracer.currentSpan();
		if (currentSpan != null) {
			return currentSpan.context().toString();
		}

		return tracer.nextSpan().context().toString();

	}

}
