package com.nrk.springexamples.common.rest.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nrk.springexamples.common.service.TracerService;
import com.nrk.springexamples.common.service.VersionService;
import com.nrk.springexamples.common.utils.ServerUtils;

@RestController
@RequestMapping("system")
public class ApplicationHealthController {

	@Autowired
	private Environment environment;
	@Autowired
	private TracerService tracerService;
	@Autowired
	private VersionService versionService;

	
	@GetMapping
	public Map<String, Serializable> applicationDeploymentDetails() throws Exception {
		final Map<String, Serializable> response = new HashMap<>();
		response.put("appId", environment.getRequiredProperty("spring.application.name"));
		response.put("status", "OK");
		response.put("server",ServerUtils.getHostName());
		response.put("TraceId", tracerService.getTraceId());
		response.put("ApplicationDetails",versionService.applicationDetails());
		return response;
	}
	
	@GetMapping("git")
	public ResponseEntity<String> isValidCurrentGitCommitId(@RequestParam(name = "id") String gitCommitId) throws Exception {
		Properties applicationDetails = versionService.applicationDetails();
		final String currentGitCommitId = applicationDetails.getProperty("git.commit.id");
		if(currentGitCommitId.equals(gitCommitId)) {
			return ResponseEntity.ok(currentGitCommitId);
		}
		
		throw new IllegalArgumentException("Invalid git.commit.id:" + gitCommitId + ". Deployed git-commit-id:"+currentGitCommitId );
	}
	
	


}
