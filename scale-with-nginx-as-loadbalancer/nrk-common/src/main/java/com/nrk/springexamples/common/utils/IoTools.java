package com.nrk.springexamples.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class IoTools {
    private static final Logger LOG = LoggerFactory.getLogger(IoTools.class);

    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    protected IoTools() {
        throw new UnsupportedOperationException();
    }

    public static void closeQuietly(final InputStream inputStream) {
        try {
            inputStream.close();
        } catch (final IOException e) {
            LOG.warn("Exception occured while closing inputstream.", e);
        }
    }

    public static boolean isExists(final String path) throws Exception {
        return new ClassPathResourceLoader().isResourceExists(path);
    }

    public static InputStream getResourceAsStream(final String path) throws Exception {
        return new ClassPathResourceLoader().getResourceAsStream(path);
    }

    public static InputStream getResourceAsStream(final URL url) {
        return new ClassPathResourceLoader().getResourceAsStream(url);
    }

    public static URL getResourceUrl(final String path) throws Exception {
        return new ClassPathResourceLoader().getResourceUrl(path);
    }

    public static String getResourceAsString(final String path) throws Exception {
        final InputStream inputStream = getResourceAsStream(path);
        try {
            return toString(inputStream);
        } finally {
            closeQuietly(inputStream);
        }
    }

    public static String getResourceAsString(final URL url) {
        final InputStream inputStream = getResourceAsStream(url);
        try {
            return toString(inputStream);
        } finally {
            closeQuietly(inputStream);
        }
    }

    public static String toString(final InputStream inputStream) {
        final StringWriter sw = new StringWriter(DEFAULT_BUFFER_SIZE);
        copy(new InputStreamReader(inputStream, Charset.forName("UTF-8")), sw);
        return sw.toString();
    }

    private static void copy(final Reader reader, final Writer writer) {
        try {
            final char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            while (true) {
                final int n = reader.read(buffer);
                if (n == -1) {
                    break;
                }
                writer.write(buffer, 0, n);
            }
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

}
