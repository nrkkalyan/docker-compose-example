package com.nrk.omega.report;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestReportGeneration {

	public static void main(String[] args) throws Exception {
		StopWatch stopWatch = new StopWatch();
		try {
			stopWatch.start();

			String json = "{" + "\"template\": \"\\\\example\\\\template.html\"," + "\"data\": {"
					+ "\"title\": \"My test template\"" + "}}";

			final String uri = "http://localhost:5050/report/generate";

			final Request post = Request.Post(uri);
			for (int i = 0; i < 5; i++) {
				final InputStream content = post.bodyString(json, ContentType.APPLICATION_JSON).execute()
						.returnContent().asStream();

				final File createTempFile = File.createTempFile("omega-", "-" + i + ".pdf");
				FileUtils.copyInputStreamToFile(content, createTempFile);
				System.out.println("File created: " + createTempFile.getAbsolutePath());
			}
		} finally {
			stopWatch.stop();
			log.info(String.format("PDF generated in %s s", stopWatch.getTotalTimeSeconds()));
		}
	}

}
