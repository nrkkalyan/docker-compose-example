package com.nrk.springexamples.weekday.rest.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nrk.springexamples.common.service.TracerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("weekday")
@Slf4j
public class WeekDayController {

	@Autowired
	private Environment environment;

	@Autowired
	private TracerService tracerService;

	@GetMapping(path = "of")
	public Map<String, Serializable> getWeekDay(
			@RequestParam(value = "date") @DateTimeFormat(iso = ISO.DATE) final LocalDate date) {
		log.info("Calling getWeekDay..");

		if (date.isAfter(LocalDate.now())) {
			throw new RuntimeException("Date must be before the current date:" + LocalDate.now());
		}

		final Map<String, Serializable> response = new HashMap<>();
		response.put("DayOfWeek", date.getDayOfWeek());
		response.put("Country", environment.getRequiredProperty("app.country"));
		response.put("Port", environment.getRequiredProperty("server.port"));
		response.put("TraceId", tracerService.getTraceId());
		response.put("appId", environment.getRequiredProperty("spring.application.name"));
		return response;
	}

}
