package com.nrk.springexamples.weekday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.nrk.springexamples"})
public class WeekDayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeekDayServiceApplication.class, args);
	}

}
